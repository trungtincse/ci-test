accessToken = $.cookie("accessToken");
name = $.cookie("name");
avatar_url = $.cookie("ava_url");



function deleteServer(id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      let settings = {
        url: `${window.prefixPathHref}/admin/log/${id}`,
        method: 'DELETE',
        timeout: 0
      }
      $.ajax(settings).done(response => {
        Swal.fire(
          'Deleted!',
          '',
          'success'
        ).then(function () {
          location.reload();
        });
      }).fail(message => {
        Swal.fire(
          'Request is not success!',
          '',
          'error'
        )
      });
    }
  })
}


function deleteService(service_id) {

  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      let settings = {
        url: `${window.prefixPathHref}/admin/log/${service_id}`,
        method: 'DELETE',
        timeout: 0
      }
      $.ajax(settings).done(response => {
        Swal.fire(
          'Deleted!',
          '',
          'success'
        ).then(function () {
          window.location = currURI();
        });
      }).fail(message => {
        Swal.fire(
          'Request is not success!',
          '',
          'error'
        )
      });
    }
  });
}

function currURI() {
  let curr_page = $.cookie('curr-page')
  if (curr_page == 'logmanagement') {
    return `${window.prefixPathHref}/admin/log`;
  }
  else if (curr_page == 'userpage') {
    return `${window.prefixPathHref}/log`;
  }
  else {
    return `${window.prefixPathHref}/`;
  }
}

function deleteSubsUser(service_id, uid) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, remove user!'
  }).then((result) => {
    if (result.value) {
      let settings = {
        url: `${window.prefixPathHref}/admin/log/${service_id}/${uid}`,
        method: 'DELETE',
        timeout: 0
      }
      $.ajax(settings).done(response => {
        Swal.fire(
          'Deleted!',
          '',
          'success'
        ).then(function () {
          location.reload();
        });
      }).fail(message => {
        Swal.fire(
          'Request is not success!',
          '',
          'error'
        )
      });
    }
  });
}

function deleteUser(uid) {
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete user!'
  }).then((result) => {
    if (result.value) {
      let settings = {
        url: `${window.prefixPathHref}/admin/user/${uid}`,
        method: 'DELETE',
        timeout: 0
      }
      $.ajax(settings).done(response => {
        Swal.fire(
          'Deleted!',
          '',
          'success'
        ).then(function () {
          location.reload();
        });
      }).fail(message => {
        Swal.fire(
          'Request is not success!',
          '',
          'error'
        )
      });
    }
  });
}

$( document ).ready(function() {
  var Shuffle = window.Shuffle;
  var sizer = $('.my-sizer-element');
  var myShuffle = new Shuffle(document.querySelector('.my-shuffle'), {
    itemSelector: '.js-item',

    // sizer: sizer,
    // roundTransforms: true,
    // isCentered: false
    // buffer: 1,
    // delimeter: ','
  });
});

$('#look-up-input').keyup(lookup);

function lookup() {
  var value = $('#look-up-input').val();
  // console.log(value)
  // console.log($('#look-up-input').val())
  myShuffle.filter(function (element) {
    group = $(element).data("groups").toString()
    console.log(group)
    return group.indexOf(value) !== -1;
  });



}
