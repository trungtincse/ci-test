FROM harbor.infra.zalo.services/za/java:8u192-jdk
 
COPY docker/supervisord/conf.d/ /zserver/supervisord/conf.d
 
COPY . ./
 
EXPOSE 80
